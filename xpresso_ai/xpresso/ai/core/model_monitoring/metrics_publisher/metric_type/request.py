"""
    Module that defines model request schema
"""

__all__ = ["Request"]
__author__ = ["Shlok Chaudhari"]


from xpresso.ai.core.commons.utils.xpresso_base_enum import BaseEnum


class Request(BaseEnum):
    """
        Class that lists down all the labels
        for model request schema
    """
    MODEL_ID = "model_id"
    MODEL_REQUEST_ID = "model_request_id"
    TYPE = "type"
    TIME = "time"
    REQUEST_TYPE = "request_type"
    REQUEST_PATH = "request_path"
    REQUEST_METHOD = "request_method"
    REQUEST_HEADERS = "request_headers"
    REQUEST_PARAMETER = "request_parameter"
    REQUEST_DATA = "request_data"
    REQUEST_SIZE_BYTES = "request_size_bytes"
